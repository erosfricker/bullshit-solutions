package ch.bullshitsolutions.bullshit_detector.app;

import android.app.Activity;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.db.chart.model.LineSet;
import com.db.chart.model.Point;
import com.db.chart.view.ChartView;
import com.db.chart.view.LineChartView;
import com.db.chart.view.animation.Animation;
import com.db.chart.view.animation.easing.BounceEase;
import com.db.chart.view.animation.easing.LinearEase;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link AnalyticsFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link AnalyticsFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class AnalyticsFragment extends Fragment{
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER


    private static final int MIN_VALUE_Y = 0;
    private static final int MAX_VALUE_Y = 100;
    private static final int STEP_VALUE_Y = 10;


    private LineChartView lineGraphView;
    private LineSet resultDataset;
    private TextView headerTextView;


    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment AnalyticsFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static AnalyticsFragment newInstance(String param1, String param2) {
        AnalyticsFragment fragment = new AnalyticsFragment();
        return fragment;
    }

    public AnalyticsFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view =  inflater.inflate(R.layout.fragment_analytics, container, false);

        Typeface fontBlenderPro = Typeface.createFromAsset(getActivity()
                .getApplicationContext()
                .getAssets(), "fonts/blenderpro/blenderpro-bold-webfont.ttf");
        this.headerTextView = (TextView) view.findViewById(R.id.header_textview);
        headerTextView.setTypeface(fontBlenderPro);
        lineGraphView = (LineChartView)view.findViewById(R.id.linechart);

        String bullshitsJson = PreferenceManager.getDefaultSharedPreferences(getActivity()).getString("bullshits", null);
        if (bullshitsJson == null){
            return view;
        }
        Type type = new TypeToken<ArrayList<Integer>>(){}.getType();
        List<Integer> bullshitScoreList = new Gson().fromJson(bullshitsJson, type);
        resultDataset = new LineSet();

        for (Integer i : bullshitScoreList) {
            resultDataset.addPoint(new Point("", i));
        }
        resultDataset.setColor(getResources().getColor(R.color.graph_line_color));
        resultDataset.setDotsColor(getResources().getColor(R.color.main_background_color));
        resultDataset.setDotsRadius(20.0f);
        resultDataset.setShadow(0.5f, 0.2f, 0.4f, getResources().getColor(R.color.button_material_dark));
        resultDataset.setDotsStrokeColor(getResources().getColor(R.color.graph_line_color));
        resultDataset.setSmooth(true);
        resultDataset.setFill(getResources().getColor(R.color.main_background_color));
        lineGraphView.addData(resultDataset);
        lineGraphView.setVisibility(View.VISIBLE);
        lineGraphView.setAxisBorderValues(MIN_VALUE_Y, MAX_VALUE_Y, STEP_VALUE_Y);
        lineGraphView.setAxisColor(getResources().getColor(R.color.graph_axis_color));
        lineGraphView.setLabelsColor(getResources().getColor(R.color.graph_line_color));
        Paint gridPaint = new Paint();
        gridPaint.setColor(getResources().getColor(R.color.graph_line_color));
        lineGraphView.setGrid(ChartView.GridType.HORIZONTAL, gridPaint);
        lineGraphView.setXAxis(false);


        lineGraphView.show();


        return view;

    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (OnFragmentInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        public void onFragmentInteraction(Uri uri);
    }

}
