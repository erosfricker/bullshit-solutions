package ch.bullshitsolutions.bullshit_detector.app;

import android.app.Activity;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.TextViewCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link GaugeFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link GaugeFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class GaugeFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    private CustomGauge gauge;
    private TextView gaugeLabel;
    private double bullshitScore = 0;
    private TextView headerTextView;

    private OnFragmentInteractionListener mListener;

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment GaugeFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static GaugeFragment newInstance(String param1, String param2) {
        GaugeFragment fragment = new GaugeFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    public GaugeFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view =  inflater.inflate(R.layout.fragment_gauge, container, false);
        Typeface fontBlenderPro = Typeface.createFromAsset(getActivity()
                .getApplicationContext()
                .getAssets(), "fonts/blenderpro/blenderpro-bold-webfont.ttf");
        this.headerTextView = (TextView) view.findViewById(R.id.header_textview);
        headerTextView.setTypeface(fontBlenderPro);
        gaugeLabel = (TextView)view.findViewById(R.id.gaugeLabel);
        gauge = (CustomGauge)view.findViewById(R.id.gauge);
        bullshitScore = this.getArguments().getDouble("SCORE");
        if (bullshitScore >0) {
            animateGauge();
        }

        return view;
    }

    private void animateGauge() {
        new Thread() {
            public void run() {
                int i;
                for (i=0;i<=bullshitScore;i++) {
                    try {
                        final int score = i;
                        getActivity().runOnUiThread(new Runnable() {

                            @Override
                            public void run() {

                                gauge.setValue(score*5);
                                gaugeLabel.setText(String.valueOf(score)+"%");
                            }
                        });
                        Thread.sleep(50);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        }.start();

    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (OnFragmentInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        public void onFragmentInteraction(Uri uri);
    }

}
