package ch.bullshitsolutions.bullshit_detector.app;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.preference.PreferenceManager;
import android.speech.RecognizerIntent;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import org.apache.commons.lang3.StringUtils;

import java.io.*;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

/**
 * A placeholder fragment containing a simple view.
 */
public class RecordSpeechFragment extends Fragment {

    public RecordSpeechFragment() {
    }
    private Button recordButton;
    private Button analysisResultButton;
    private Button showChartButton;

    private TextView headerTextView;
    private TextView recordedTextView;
    private static final int CODE_SPEECH_INPUT_COMPLETE = 100;

    private Double score = 0.0;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        Typeface fontBlenderPro = Typeface.createFromAsset(getActivity()
                .getApplicationContext()
                .getAssets(), "fonts/blenderpro/blenderpro-bold-webfont.ttf");

        View view = inflater.inflate(R.layout.fragment_record_speech, container, false);

        this.headerTextView = (TextView) view.findViewById(R.id.header_textview);
        headerTextView.setTypeface(fontBlenderPro);
        this.recordButton = (Button) view.findViewById(R.id.record_button);
        this.recordedTextView = (TextView)view.findViewById(R.id.textview_recorded_text);
        recordButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                recordSpeech();
            }
        });
        analysisResultButton = (Button)view.findViewById(R.id.analysis_result_button);
        analysisResultButton.setVisibility(View.INVISIBLE);
        analysisResultButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                GaugeFragment gaugeFragment = new GaugeFragment();
                Bundle extras = new Bundle();
                extras.putDouble("SCORE", score);
                gaugeFragment.setArguments(extras);
                replaceFragment(gaugeFragment);
            }
        });

        showChartButton = (Button)view.findViewById(R.id.show_chart_button);
        showChartButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                replaceFragment(new AnalyticsFragment());
            }
        });

        return view;
    }

    private void replaceFragment(Fragment fragment) {
        FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.container, fragment);
        transaction.addToBackStack(null);
        transaction.commit();
    }

    private void recordSpeech() {
        Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL, RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, Locale.US);
        intent.putExtra(RecognizerIntent.EXTRA_PROMPT, getString(R.string.record_prompt));

        try {
            startActivityForResult(intent, CODE_SPEECH_INPUT_COMPLETE);
        }catch (ActivityNotFoundException e) {
            Toast.makeText(getActivity(), "Speech is not supported", Toast.LENGTH_LONG).show();
        }

    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case CODE_SPEECH_INPUT_COMPLETE:

                if (resultCode == Activity.RESULT_OK && data != null) {
                    analysisResultButton.setVisibility(View.VISIBLE);
                    ArrayList<String> result = data.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
                    recordedTextView.setText(recordedTextView.getText() + " " + result.get(0));
                    analyzeText(result.get(0));
                    saveBullshitScore();
                }

                break;
            default:
                break;
        }
    }

    private void analyzeText(String textToAnalyze) {

        ArrayList<String> bullshitWords = null;
        try {
            bullshitWords = getBullshitList();
        } catch (IOException e) {
            e.printStackTrace();
        }

        for (String s : bullshitWords) {

                textToAnalyze = textToAnalyze.replaceAll(s, "+");

        }
        int bullshitCount = StringUtils.countMatches(textToAnalyze, "+");
        int wordCount = textToAnalyze.split(" ").length;


        System.out.println("bullshitWords length is " + bullshitWords.size());
        System.out.println("WordCount is: " + wordCount);
        System.out.println("BullshitCount is: " + bullshitCount);

        if (wordCount > 0) {
            score = (((double) bullshitCount) / wordCount) * 100;
        }
        System.out.println("BullshitScore is: " + score + " %");




    }

    private void saveBullshitScore() {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getActivity().getApplicationContext());
        String bullshitsJson =preferences.getString("bullshits", null);
        if (bullshitsJson==null){
            ArrayList<Integer> bullshitScores = new ArrayList<>();
            bullshitScores.add(score.intValue());
            SharedPreferences.Editor editor = preferences.edit();
            editor.putString("bullshits", new Gson().toJson(bullshitScores));
            editor.commit();


        }else {
            Type type = new TypeToken<ArrayList<Integer>> (){}.getType();
            List<Integer> bullshitScoreList = new Gson().fromJson(bullshitsJson, type);
            bullshitScoreList.add(score.intValue());
            SharedPreferences.Editor editor = preferences.edit();
            editor.putString("bullshits", new Gson().toJson(bullshitScoreList));
            editor.commit();
        }

    }

    private ArrayList<String> getBullshitList() throws IOException {

        InputStream rawFile = getResources().openRawResource(R.raw.bullshit);

        BufferedReader br = null;
        String line = "";
        String cvsSplitBy = ",";
        ArrayList<String> bullshitWords = new ArrayList<String>();

        try {

            br = new BufferedReader(new InputStreamReader(rawFile));
            while ((line = br.readLine()) != null) {

                // use comma as separator
                String[] bullshit = line.split(cvsSplitBy);

                for (String bs : bullshit) {

                    bullshitWords.add(bs.toLowerCase().trim());
                }

            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return bullshitWords;
    }


}
